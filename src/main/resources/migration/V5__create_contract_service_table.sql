CREATE TABLE contract_service (
    contract_id INT,
    service_id INT,
    PRIMARY KEY (contract_id, service_id)
);