ALTER TABLE clients MODIFY id INT AUTO_INCREMENT;
CREATE TABLE contracts (
    id INT PRIMARY KEY,
    name VARCHAR(128) NOT NULL,
    client_id INT,
    FOREIGN KEY (client_id) REFERENCES clients (id)
);