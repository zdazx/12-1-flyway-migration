CREATE TABLE clients (
    id INT PRIMARY KEY,
    fullName VARCHAR(128) NOT NULL,
    abbreviation VARCHAR(6) NOT NULL,
    type VARCHAR(8)
);