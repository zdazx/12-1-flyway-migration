ALTER TABLE contract_service
    ADD FOREIGN KEY (contract_id) REFERENCES contracts(id);

ALTER TABLE contract_service
    ADD FOREIGN KEY (service_id) REFERENCES services(id);

ALTER TABLE contract_service
    ADD COLUMN admin_id INT,
    ADD FOREIGN KEY (admin_id) REFERENCES staff (id);