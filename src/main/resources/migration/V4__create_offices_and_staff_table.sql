CREATE TABLE offices(
    id INT AUTO_INCREMENT,
    country VARCHAR(32) NOT NULL,
    city VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE staff(
    id INT AUTO_INCREMENT,
    first_name VARCHAR(32) NOT NULL,
    last_name VARCHAR(32) NOT NULL,
    office_id INT,
    PRIMARY KEY (id),
    FOREIGN KEY (office_id) REFERENCES offices(id)
);