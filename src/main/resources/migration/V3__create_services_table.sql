CREATE TABLE services(
    id INT AUTO_INCREMENT,
    name VARCHAR(64),
    PRIMARY KEY (id)
);

ALTER TABLE contracts
    ADD COLUMN service_id INT,
    ADD FOREIGN KEY (service_id) REFERENCES services(id);